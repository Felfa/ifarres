class plugin(object):
    # Loads the text strings from the bots language #
    def __init__(self, bot):
        self.bot = bot
        self.commands = self.bot.trans.plugins.getid.commands
        self.description = self.bot.trans.plugins.getid.description

    # Plugin action #
    def run(self, m):
        if m.reply:
            uname = m.reply.sender.first_name
            uid = m.reply.sender.id
        else:
            uname = m.sender.first_name
            uid = m.sender.id
            
        if m.conversation.id < 0:
            gname = m.conversation.title
            gid = m.conversation.id  * -1
        else:
            gname = None
            gid = None
            
        text = "👤 *" + uname + "* \n" + "`" + str(uid) + "`" + "\n\n" + "👥 *" + str(gname) + "* \n" + "`" + str(gid) + "`"
        
        return self.bot.send_message(m, text, extra={'format': 'Markdown'})
