﻿from polaris.utils import get_input, is_command, send_request, download, remove_html


class plugin(object):
    # Loads the text strings from the bots language #
    def __init__(self, bot):
        self.bot = bot
        self.commands = self.bot.trans.plugins.weather.commands
        self.description = self.bot.trans.plugins.help.description

    # Plugin action #
    def run(self, m):
        input = get_input(m, ignore_reply=False)
        if not input:
            return self.bot.send_message(m, self.bot.trans.errors.missing_parameter, extra={'format': 'HTML'})

        query = "select * from weather.forecast where woeid in (SELECT woeid FROM geo.places WHERE text='%s') and u = 'c'" % input
        url = 'https://query.yahooapis.com/v1/public/yql?q=%s&format=json&env=store://datatables.org/alltableswithkeys' % query

        data = send_request(url)

        try:
            results = data.query.count
        except:
            return self.bot.send_message(m, self.bot.trans.errors.connection_error)
            
        if results == 0:
            return self.bot.send_message(m, self.bot.trans.errors.no_results)
        elif results == 1:
            weather = data.query.results.channel
        else:
            weather = data.query.results.channel[0]
            
        try:
            title = self.bot.trans.plugins.weather.strings.title % (weather.location.city, weather.location.country)
        except:
            return self.bot.send_message(m, self.bot.trans.errors.no_results)
            
        temp = weather.item.condition.temp
        condition = weather.item.condition.text.lower()

        weather_string = self.bot.trans.plugins.weather.strings[condition]
        weather_icon = (self.get_weather_icon(condition.lower()))
        humidity = weather.atmosphere.humidity
        wind = weather.wind.speed

        if is_command(self, 1, m.content):
            message = u'%s\n%s %s\n🌡%sºC 💧%s 🌬%s km/h' % (
                remove_html(title), weather_icon, weather_string, temp, humidity, wind)
        elif is_command(self, 2, m.content):
            try:
                forecast = weather.item.forecast
            except:
                return self.bot.send_message(m, self.bot.trans.weather.no_forecasts)
                
            message = self.bot.trans.plugins.weather.strings.titleforecast % (weather.location.city, weather.location.country)
            for day in forecast:
                weekday = self.bot.trans.plugins.weather.strings[day.day.lower()]
                numday = day.date[:2].strip()
                temp = day.low
                temp_max = day.high
                # weather_string = day.conditions.title()
                weather_string = self.bot.trans.plugins.weather.strings[day.text.lower()]
                weather_icon = (self.get_weather_icon(day.text.lower()))
                message += u'\n • <b>%s %s</b>: 🌡 %s-%sºC %s %s' % (weekday, numday, temp, temp_max, weather_icon, weather_string)

        return self.bot.send_message(m, message, extra={'format': 'HTML'})

    @staticmethod
    def get_weather_icon(icon):
        weather_emoji = {}
        weather_emoji['clear'] = u'☀️'
        weather_emoji['tornado'] = u'🌪'
        weather_emoji['tropical storm'] = u'⛈'
        weather_emoji['hurricane'] = u'🌪'
        weather_emoji['severe thunderstorms'] = u'⚡️'
        weather_emoji['thunderstorms'] = u'🌩'
        weather_emoji['mixed rain and snow'] = u'🌧❄️️️'
        weather_emoji['mixed rain and sleet'] = u'🌧'
        weather_emoji['mixed snow and sleet'] = u'🌨'
        weather_emoji['freezing drizzle'] = u'🌧'
        weather_emoji['drizzle'] = u'🌧'
        weather_emoji['freezing rain'] = u'🌧'
        weather_emoji['showers'] = u'🌧'
        weather_emoji['snow flurries'] = u'❄️️'
        weather_emoji['rain'] = u'🌧'
        weather_emoji['light snow showers'] = u'🌨️️'
        weather_emoji['blowing snow'] = u'💨❄️️️'
        weather_emoji['snow'] = u'💨❄️️️'
        weather_emoji['hail'] = u'🌨'
        weather_emoji['sleet'] = u'🌧❄️'
        weather_emoji['dust'] = u'🌫'
        weather_emoji['foggy'] = u'🌫'
        weather_emoji['haze'] = u'🌞'
        weather_emoji['smoky'] = u'🌫'
        weather_emoji['blustery'] = u'💨'
        weather_emoji['windy'] = u'💨'
        weather_emoji['breezy'] = u'💨'
        weather_emoji['cold'] = u'⛄️'
        weather_emoji['cloudy'] = u'☁️'
        weather_emoji['mostly cloudy'] = u'☁️'
        weather_emoji['partly cloudy'] = u'⛅️'
        weather_emoji['sunny'] = u'☀️'
        weather_emoji['fair'] = u'🌤'
        weather_emoji['mostly sunny'] = u'🌤'
        weather_emoji['mixed rain and hail'] = u'🌧❄️'
        weather_emoji['hot'] = u'🔥'
        weather_emoji['isolated thunderstorms'] = u'🌩'
        weather_emoji['scattered thunderstorms'] = u'🌩'
        weather_emoji['scattered showers'] = u'🌦'
        weather_emoji['heavy snow'] = u'❄️️️'
        weather_emoji['scattered snow showers'] = u'❄️️️'
        weather_emoji['thundershowers'] = u'⛈'
        weather_emoji['snow showers'] = u'🌨️️'
        weather_emoji['isolated thundershowers'] = u'⛈'
        weather_emoji['not available'] = u'❓'
        
        return weather_emoji[icon]
